package com.mycompany.app;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Hello world!
 *
 */
public class App
{
  public static void main(String[] args) {
    Injector injector = Guice.createInjector(new AppModule());
    AlgoVisualizer visualizer = injector.getInstance(AlgoVisualizer.class);
    visualizer.start();
  }
}
