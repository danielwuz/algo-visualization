package com.mycompany.app;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**  */
public class AppModule extends AbstractModule {

  @Override
  public void configure() {
    bind(String.class).annotatedWith(Names.named("title")).toInstance("Welcome");
    bind(Integer.class).annotatedWith(Names.named("width")).toInstance(3000);
    bind(Integer.class).annotatedWith(Names.named("height")).toInstance(1000);
  }
}
